# test BLE Scanning software
# jcs 6/8/2014


import sys, os
import datetime, time

import bluetooth._bluetooth as bluez
from iBeaconScanner import blescan

from m2x.client import M2XClient

# device configuration is read from system envrionment varialles
API_KEY = os.environ.get('M2X_KEY')
DEVICE_ID = os.environ.get('M2X_DEVICE_ID')
STREAM = os.environ.get('M2X_STREAM')

if not API_KEY:
	print API_KEY
	print "error: no or empty M2X_KEY system variable set"
	sys.exit(1)

if not DEVICE_ID:
	print "error: no or empty M2X_DEVICE_ID system variable set"
	sys.exit(1)

if not STREAM:
	print "error: no or empty M2X_STREAM system variable set"
	sys.exit(1)

# prepare M2X device
client = M2XClient(key=API_KEY)
device = client.device(DEVICE_ID)
stream = device.stream(STREAM)

# Init BLE
dev_id = 0

try:
	sock = bluez.hci_open_dev(dev_id)
	#print "ble thread started"

except:
	print "error accessing bluetooth device..."
	sys.exit(1)

blescan.hci_le_set_scan_parameters(sock)
blescan.hci_enable_le_scan(sock)

while True:
	returnedList = blescan.parse_events(sock, 10)
	values = []

	for beacon in returnedList:
		# some Beacons are detected prepare data for M2X
		uid = beacon.split(',')[1]

		values.append(dict(
			timestamp=datetime.datetime.now(),
			value=uid
		))


	# push beacon uuid to M2X
	device.post_updates(values={STREAM: values})
	time.sleep(3)
