# WasThere 

WasThere is project aiming to build M2M platform to help you organize your life.
It allows you to register custom device and get notification when it meets preset point of interest (iBeacon, rf-id tag, ...). The backbone of this project is [AT&T M2X Cloud](https://m2x.att.com). 

## Current status:
- rapsberry pi 3
- iBeacon
- series of cloud services

### Android application
To receive push notifications as a reminder when registered device meets point of interest.

### Rapsberry pi 3
Is able to detect iBeacon (tested using [Kontakt.io](http://kontakt.io/)) reports it to M2X. See [wasthere-raspberry](https://bitbucket.org/vojtisek_michal/wasthere-raspberry) for details.

## Setting up
### Help script to post data to M2X stream
Set your credentials, device and stream into system environment:
```
export M2X_KEY=YOUR_M2X_API_KEY
export M2X_DEVICE_ID=YOUR_M2X_DEVICE_ID
export M2X_STREAM=YOUR_M2X_STREAM_ID
```
Then you can post any data to the M2X:
```
$ ./post_m2x_data.sh some-data-value
```

## AppEngine backend
To buid the backend module: put your OneSignal credentials into config file at /android/backend/src/main/resources/config.properties. You can rename config.properties.example and fill in your details. 

## Authors
 * Michal Vojtíšek <vojtisek.michal@gmail.com>
 * Petr Martinec <petr.martinec@gmail.com>