package cz.vojtisek.hackathon.wasthere.backend;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cz.vojtisek.hackathon.wasthere.Proto;

public class BeaconServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        getKnownBeacons()
                .writeTo(resp.getOutputStream());
    }

    public static Proto.Beacons getKnownBeacons() {
        return Proto.Beacons.newBuilder()
                .addBeacons(Proto.Beacon.newBuilder()
                        .setUuid("D9:94:19:BB:6F:4B")
                        .setName("at home"))
                .addBeacons(Proto.Beacon.newBuilder()
                        .setUuid("CB:E0:03:B3:4B:52")
                        .setName("at playground"))
                .addBeacons(Proto.Beacon.newBuilder()
                        .setUuid("C2:88:AB:0B:1D:10")
                        .setName("at classroom"))
                .addBeacons(Proto.Beacon.newBuilder()
                        .setUuid("DA:43:6C:31:FE:F6")
                        .setName("at grandma's place"))
                .build();
    }
}
