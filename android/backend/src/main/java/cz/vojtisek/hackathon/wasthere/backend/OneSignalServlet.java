package cz.vojtisek.hackathon.wasthere.backend;

import com.google.appengine.repackaged.com.google.gson.JsonObject;
import com.google.appengine.repackaged.com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cz.vojtisek.hackathon.wasthere.Proto;

public class OneSignalServlet extends HttpServlet {

    private static final String ONESIGNAL_URL = "https://onesignal.com/api/v1/notifications";

    private String postToOpenSignal(String onesignalApiKey, String onesignalAppId, String extraData) throws IOException {

        URL obj = new URL(ONESIGNAL_URL);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("POST");
        con.setRequestProperty("Host", "onesignal.com:443");
        con.setRequestProperty("Authorization", "Basic " + onesignalApiKey);

        String urlParameters = "app_id=" + onesignalAppId + "&isAndroid=true&android_background_data=true&included_segments[]=All" + extraData;
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter out = resp.getWriter();
        out.println("Go home, you are drunk!");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        PrintWriter out = resp.getWriter();

        JsonObject result = new JsonParser().parse(req.getReader()).getAsJsonObject();
        String value = result.getAsJsonObject("values").getAsJsonObject("seen").getAsJsonPrimitive("value").getAsString();

        Properties prop = new Properties();
        prop.load(getClass().getClassLoader().getResourceAsStream("config.properties"));

        List<Proto.Beacon> beaconsList = BeaconServlet.getKnownBeacons().getBeaconsList();
        for (Proto.Beacon beacon: beaconsList) {
            if (value.equalsIgnoreCase(beacon.getUuid())) {
                String response = postToOpenSignal(prop.getProperty("ONESIGNAL_API_KEY"), prop.getProperty("ONESIGNAL_APP_ID"),
                        "&data[beacon_name]=" + beacon.getName());
                out.print(response);
                return;
            }
        }
        out.println("Nothing to do here, goodbye!");
    }
}
