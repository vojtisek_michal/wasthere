# http://developer.kontakt.io/android-sdk/2.1.0/quickstart/
# -keep class com.squareup.okhttp.** { *; }
# -keep interface com.squareup.okhttp.** { *; }
# -dontwarn com.squareup.okhttp.**
# -dontwarn okio.**

# -keep class com.kontakt.sdk.** { *; }
# -keep interface com.kontakt.sdk.** { *; }