package cz.vojtisek.hackathon.wasthere;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    //private static final String BACKEND_URL = "http://10.0.200.164:8080";
    private static final String BACKEND_URL = "http://v4-hackathon-publicspy.appspot.com";
    private Proto.Beacons mBeacons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new LoadBeaconsTasks(this).execute();
    }

    private void drawBeacons(Proto.Beacons beacons) {
        mBeacons = beacons;
    }

    private class LoadBeaconsTasks extends AsyncTask<Void, Void, Proto.Beacons> {

        private WeakReference<MainActivity> activityWeakReference;

        public LoadBeaconsTasks(MainActivity activity) {
            activityWeakReference = new WeakReference<>(activity);
        }

        @Override
        protected Proto.Beacons doInBackground(Void... params) {
            try {
                URL url = new URL(BACKEND_URL + "/beacon");
                BufferedInputStream bis = new BufferedInputStream(url.openConnection().getInputStream());
                try {
                    return Proto.Beacons.parseFrom(bis);
                }finally {
                    bis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Proto.Beacons beacons) {
            if ((beacons != null) && (activityWeakReference.get() != null)) {
                activityWeakReference.get().drawBeacons(beacons);
            }
        }
    }
}