package cz.vojtisek.hackathon.wasthere;

import android.app.Application;

import com.onesignal.OneSignal;

public class WasthereApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        OneSignal.startInit(this)
                .init();
    }
}
