#!/bin/sh

FILEDIR=$(dirname $0)

SRC_DIR="$FILEDIR/proto"
DST_DIR="$FILEDIR/../shared/src/gen/java/"

rm $DST_DIR/* -rf

protoc -I=$SRC_DIR --java_out=$DST_DIR $SRC_DIR/wasthere.proto