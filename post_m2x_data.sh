#!/bin/sh

current_date=$(date --utc +%FT%TZ)
data='{"values":[{"timestamp":"'${current_date}'", "value":"'$1'"}]}'

# echo $data

curl -s -H "X-M2X-KEY: $M2X_KEY" \
    -H "Content-Type: application/json" \
    -XPOST -d "$data" \
    http://api-m2x.att.com/v2/devices/$M2X_DEVICE_ID/streams/$M2X_STREAM/values
